public class Board{
	//tic tac toe grid represented by a Tile 2D array
	private Tile[][] grid;
	//variable that represents the size of the board
	private final int boardSize = 3;
	//Board constructor that intializes the grid array and fils it with blank tiles
	public Board(){
		grid = new Tile[boardSize][boardSize];
		for(int i=0; i<grid.length; i++){
			for(int j=0; j<grid[i].length; j++){
				grid[i][j] = Tile.BLANK;
			}
		}
	}
	//overriding toString method so that the board will be in the shape of a 3 x 3 grid when printed
	public String toString(){
		String output = "";
		for(int i=0; i<this.grid.length; i++){
			for(int j=0; j<this.grid[i].length; j++){
				output += this.grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;
	}
	
	//placeToken method that places a token on the board, takes in the row and column of the tile, as well as the playerToken, and returns true if you can place a tile in that location. Returns false if the tile is already occupied or if the row and column are not within the board's space
	public boolean placeToken(int row, int col, Tile playerToken){
		if(row < 0 || row > boardSize-1 || col < 0 || row > boardSize-1){
			return false;
		}
		if(this.grid[row][col] == Tile.BLANK){
			this.grid[row][col] = playerToken;
			return true;
		}
		else{
			return false;
		}
	}
	
	//checkIfFull method which checks if the entire board is full and returns true if is, false if it isn't
	public boolean checkIfFull(){
		for(int i=0; i<grid.length; i++){
			for(int j=0; j<grid[i].length; j++){
				if(grid[i][j] == Tile.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	//checkIfWinningHorizontal method that checks if the playerToken appears three times in a row horizontally, and returns true if it does, false if it doesn't
	private boolean checkIfWinningHorizontal(Tile playerToken){
		int counter = 0;
		//first for loop goes over each row, the second for loop checks each column, and if the column in that row is euqla to the player token, it increases the counter by one. If the counter is equal to the board size, or 3 tiles in a row, then it returns true. If not, it resets the counter and checks the next row, until it runs out, then it returns false.
		for(int i=0; i<grid.length; i++){
			for(int j=0; j<grid[i].length; j++){
				if(grid[i][j] == playerToken){
					counter++;
				}
				if(counter == boardSize){
					return true;
				}
			}
			counter = 0;
		}
		return false;
	}
	
	//checkIfWinningVertical method that does the same as the above method, but checks if three tokens are in a row vertically instead of horizontally.
	private boolean checkIfWinningVertical(Tile playerToken){
		int counter = 0;
		for(int i=0; i<grid.length; i++){
			for(int j=0; j<grid[i].length; j++){
				if(grid[j][i] == playerToken){
					counter++;
				}
				if(counter == boardSize){
					return true;
				}
			}
			counter = 0;
		}
		return false;
	}
	
	//checkIfWinning method that checks if either checkIfWinningHorizontal or checkIfWinningVertical are true, and returns true if one of them are, false if neither are true.
	public boolean checkIfWinning(Tile playerToken){
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)){
			return true;
		}
		else{
			return false;
		}
	}
}