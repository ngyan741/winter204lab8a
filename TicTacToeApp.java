//import scanner
import java.util.Scanner;
public class TicTacToeApp{
	public static void main (String[] args){
		Scanner reader = new Scanner(System.in);
		//greet the user
		System.out.println("Welcome to Tic Tac Toe, players!");
		//create a new board object for the game
		Board gameBoard = new Board();
		//create a boolean condition for if the game is over
		boolean gameOver = false;
		//integer that represents the current player
		int player = 1;
		//tile variable that represents the current player's token (O or X)
		Tile playerToken = Tile.X;
		//main game loop that repeats while the game isn't over
		while(gameOver != true){
			//print the board and ask the player where to place the token and take those as inputs
			System.out.println(gameBoard);
			System.out.println("-----------------------------------------------");
			System.out.println("Which row do you want to place your token, player " + player + "?");
			int rowPlacement = Integer.parseInt(reader.nextLine());
			System.out.println("Which column do you want to place your token, player " + player + "?");
			int columnPlacement = Integer.parseInt(reader.nextLine());
			//change the playerToken depending on the player number
			if(player == 1){
				playerToken = Tile.X;
			}
			else{
				playerToken = Tile.O;
			}
			//if the token placement is invalid, tell the player that it is invalid and ask where to place the token again
			if(gameBoard.placeToken(rowPlacement, columnPlacement, playerToken) == false){
				System.out.println("Invalid location, try again");
				gameBoard.placeToken(rowPlacement, columnPlacement, playerToken);
			}
			//check to see if the player has won the game, if they did then print the final board, congratulate the player and makes gameOver true which stops the game loop
			if(gameBoard.checkIfWinning(playerToken)){
				System.out.println(gameBoard);
				System.out.println("Player " + player + " wins!");
				gameOver = true;
			}
			//if not, it checks to see if the board is full. If it is, prints the final board, declares a tie, and makes gameOver = true, which stops the game loop
			else if(gameBoard.checkIfFull()){
				System.out.println(gameBoard);
				System.out.println("It's a tie!");
				gameOver = true;
			}
			//if none of the above, then the game is not over and change to the other player to continue
			else{
				player++;
				if(player > 2){
					player = 1;
				}
			}
		}
	}
}