public enum Tile{
	//Tile enum values
	BLANK("_"),
	X("X"),
	O("O");
	
	//enum's name field
	private final String name;
	//Tile enum constructor
	private Tile(String name){
		this.name = name;
	}
	//getter
	public String getName(){
		return this.name;
	}
}

